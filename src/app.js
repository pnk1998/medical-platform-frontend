import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import Home from './home/home';

import PersonContainer from './person/person-container'
import PatientContainer from './patient/patient-container'
import CareGiverContainer from './caregiver/caregiver-container'
import DoctorContainer from './doctor/doctor-container'
import MedicationContainer from './medication/medication-container'
import PageContainer from './caregiverpage/page-container'

import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';

class App extends React.Component {

    render() {

            return (
                <div className={styles.back}>
                <Router>
                    <div>
                        <NavigationBar />
                        <Switch>

                            <Route
                                exact
                                path='/'
                                render={() => <Home/>}
                            />

                            <Route
                                exact
                                path='/person'
                                render={() => <PersonContainer/>}
                            />

                            <Route
                                exact
                                path='/patient'
                                render={() => <PatientContainer/>}
                            />

                            <Route
                                exact
                                path='/caregiver'
                                render={() => <CareGiverContainer/>}
                            />
                            <Route
                                exact
                                path='/doctor'
                                render={() => <DoctorContainer/>}
                            />
                            <Route
                                exact
                                path='/medication'
                                render={() => <MedicationContainer/>}
                            />

                            <Route
                                exact
                                path='/page'
                                render={() => <PageContainer/>}
                            />


                            {/*Error*/}
                            <Route
                                exact
                                path='/error'
                                render={() => <ErrorPage/>}
                            />

                            <Route render={() =><ErrorPage/>} />
                        </Switch>
                    </div>
                </Router>
                </div>
            )
        };
}

export default App
