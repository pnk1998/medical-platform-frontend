import React from 'react';
import validate from "./validators/patient-validators"
import Button from "react-bootstrap/Button";
import * as API_USERS from "../api/patient-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';

class UpdatePatientForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                name_for_update: {
                    value: '',
                    placeholder: 'What is your name?...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                name: {
                    value: '',
                    placeholder: 'What is your name?...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                address: {
                    value: '',
                    placeholder: 'Cluj, Zorilor, Str. Lalelelor 21...',
                    valid: false,
                    touched: false,
                },
                date: {
                    value: '',
                    placeholder: '1998-10-29.....',
                    valid: false,
                    touched: false,
                },
               gender: {
                    value: '',
                    placeholder: 'male / female',
                    valid: false,
                    touched: false,
                },
                med_rec: {
                    value: '',
                    placeholder: 'Healthy, unhealthy, lightly sick......',
                    valid: false,
                    touched: false,
                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    updatePatient(name_for_update,patient) {
        return API_USERS.putPatient(name_for_update, patient,(result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully updated patient with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {

        let patient = {
            name: this.state.formControls.name.value,
            address: this.state.formControls.address.value,
            date: this.state.formControls.date.value,
            gender: this.state.formControls.gender.value,
            med_rec: this.state.formControls.med_rec.value
        };
        let name_for_update = this.state.formControls.name_for_update.value;
        this.updatePatient(name_for_update,patient);

        console.log(patient);
    }

    render() {
        return (
            <div>
                <FormGroup id='name_for_update'>
                    <Label for='name_for_updateField'> Name Update: </Label>
                    <Input name='name_for_update' id='name_for_updateField' placeholder={this.state.formControls.name_for_update.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.name_for_update.value}
                           touched={this.state.formControls.name_for_update.touched? 1 : 0}
                           valid={this.state.formControls.name_for_update.valid}
                           required
                    />
                    {this.state.formControls.name.touched && !this.state.formControls.name_for_update.valid &&
                    <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                </FormGroup>

                <FormGroup id='name'>
                    <Label for='nameField'> Name: </Label>
                    <Input name='name' id='nameField' placeholder={this.state.formControls.name.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.name.value}
                           touched={this.state.formControls.name.touched? 1 : 0}
                           valid={this.state.formControls.name.valid}
                           required
                    />
                    {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                    <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                </FormGroup>

                <FormGroup id='address'>
                    <Label for='addressField'> Address: </Label>
                    <Input name='address' id='addressField' placeholder={this.state.formControls.address.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.address.value}
                           touched={this.state.formControls.address.touched? 1 : 0}
                           valid={this.state.formControls.address.valid}
                           required
                    />
                </FormGroup>

                <FormGroup id='date'>
                    <Label for='dateField'> Date of birth: </Label>
                    <Input name='date' id='dateField' placeholder={this.state.formControls.date.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.date.value}
                           touched={this.state.formControls.date.touched? 1 : 0}
                           valid={this.state.formControls.date.valid}
                           required
                    />

                </FormGroup><FormGroup id='gender'>
                    <Label for='genderField'> Gender: </Label>
                    <Input name='gender' id='genderField' placeholder={this.state.formControls.gender.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.gender.value}
                           touched={this.state.formControls.gender.touched? 1 : 0}
                           valid={this.state.formControls.gender.valid}
                           required
                    />

                </FormGroup><FormGroup id='med_rec'>
                    <Label for='med_recField'> Medical Record: </Label>
                    <Input name='med_rec' id='med_recField' placeholder={this.state.formControls.med_rec.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.med_rec.value}
                           touched={this.state.formControls.med_rec.touched? 1 : 0}
                           valid={this.state.formControls.med_rec.valid}
                           required
                    />
                </FormGroup>

                    <Row>
                        <Col sm={{size: '5', offset: 8}}>
                            <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.handleSubmit}>  Submit </Button>
                        </Col>
                    </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        ) ;
    }
}

export default UpdatePatientForm;