import React from 'react';
import validate from "./validators/caregiver-validators"
import Button from "react-bootstrap/Button";
import * as API_USERS from "../api/caregiver-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';

class PatientCareGiver extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                name_patient: {
                    value: '',
                    placeholder: 'Patient name...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                name_caregiver: {
                    value: '',
                    placeholder: 'Care Giver name...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },

            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    insertPatientToCareGiver(name_patient,name_caregiver) {
        return API_USERS.postPatientForCareGiver(name_patient,name_caregiver, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted patient in caregiver's list");
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {

            let name_patient = this.state.formControls.name_patient.value;
            let name_caregiver = this.state.formControls.name_caregiver.value;

        console.log(name_patient,name_caregiver);
        this.insertPatientToCareGiver(name_patient,name_caregiver);
    }

    render() {
        return (
            <div>
                <FormGroup id='name_patient'>
                    <Label for='name_patientField'> Name patient: </Label>
                    <Input name='name_patient' id='name_patientField' placeholder={this.state.formControls.name_patient.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.name_patient.value}
                           touched={this.state.formControls.name_patient.touched? 1 : 0}
                           valid={this.state.formControls.name_patient.valid}
                           required
                    />
                    {this.state.formControls.name_patient.touched && !this.state.formControls.name_patient.valid &&
                    <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                </FormGroup>

                <FormGroup id='name_caregiver'>
                    <Label for='name_caregiverField'> Name caregiver: </Label>
                    <Input name='name_caregiver' id='name_caregiverField' placeholder={this.state.formControls.name_caregiver.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.name_caregiver.value}
                           touched={this.state.formControls.name_caregiver.touched? 1 : 0}
                           valid={this.state.formControls.name_caregiver.valid}
                           required
                    />
                    {this.state.formControls.name_caregiver.touched && !this.state.formControls.name_caregiver.valid &&
                    <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                </FormGroup>

                    <Row>
                        <Col sm={{size: '5', offset: 8}}>
                            <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.handleSubmit}>  Submit </Button>
                        </Col>
                    </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        ) ;
    }
}

export default PatientCareGiver;