import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
    caregiver: '/caregiver',
    caregiverPatient: '/caregiver/insertPatient/?',
    caregiverShow:'/caregiver/findPacients/'
};

function getCareGivers(callback) {
    let request = new Request(HOST.backend_api + endpoint.caregiver, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getCareGiverById(params, callback){
    let request = new Request(HOST.backend_api + endpoint.caregiver + params.id, {
       method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postCareGiver(user, callback){
    let request = new Request(HOST.backend_api + endpoint.caregiver , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deleteCareGiver(name,callback){
    let request = new Request(HOST.backend_api + endpoint.caregiver+ '/' +name, {
       method: 'DELETE'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function putCareGiver(name_for_update, user, callback){
    let request = new Request(HOST.backend_api + endpoint.caregiver +'/' + name_for_update, {
       method: 'PUT',
       headers : {
                   'Accept': 'application/json',
                   'Content-Type': 'application/json',
               },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function postPatientForCareGiver(name_patient,name_caregiver, callback){
    let request = new Request(HOST.backend_api + endpoint.caregiverPatient + "numePacient=" +name_patient+"&numeCareGiver="+name_caregiver, {
        method: 'POST',

        headers : {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                  },
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}


export {
    getCareGivers,
    getCareGiverById,
    postCareGiver,
    deleteCareGiver,
    putCareGiver,
    postPatientForCareGiver
};