import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import CareGiverForm from "./components/caregiver-form";
import DeleteCareGiverForm from "./components/delete-caregiver-form";
import UpdateCareGiverForm from "./components/update-caregiver-form";
import PatientCareGiverForm from "./components/patient-caregiver-form";

import * as API_USERS from "./api/caregiver-api"
import CareGiverTable from "./components/caregiver-table";

class CareGiverContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.toggleFormDelete = this.toggleFormDelete.bind(this);
        this.toggleFormUpdate = this.toggleFormUpdate.bind(this);
        this.toggleInsertPatient = this.toggleInsertPatient.bind(this);

        this.reload = this.reload.bind(this);
        this.reloadDelete = this.reloadDelete.bind(this);
        this.reloadUpdate = this.reloadUpdate.bind(this);
        this.reloadInsertPatient = this.reloadInsertPatient.bind(this);

        this.state = {
            selected: false,
            selectedDetele: false,
            selectedUpdate: false,
            selectedInsertPatient: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };

    }

    componentDidMount() {
        this.fetchCareGivers();
    }

    fetchCareGivers() {
        return API_USERS.getCareGivers((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    toggleFormDelete(){
        this.setState({selectedDetele: !this.state.selectedDetele});
    }

    toggleFormUpdate(){
        this.setState({selectedUpdate: !this.state.selectedUpdate});
    }

    toggleInsertPatient(){
        this.setState({selectedInsertPatient: !this.state.selectedInsertPatient});
    }


    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchCareGivers();
    }

    reloadDelete() {
        this.setState({
            isLoaded: false
        });
        this.toggleFormDelete();
        this.fetchCareGivers();
    }

    reloadUpdate() {
            this.setState({
                isLoaded: false
            });
            this.toggleFormUpdate();
            this.fetchCareGivers();
        }

    reloadInsertPatient() {
                this.setState({
                    isLoaded: false
                });
                this.toggleInsertPatient();
                this.fetchCareGivers();
            }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Care Giver Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm}>Add Care Giver </Button>
                            <Button color="primary" onClick={this.toggleFormDelete}> Delete Care Giver </Button>
                            <Button color="primary" onClick={this.toggleFormUpdate}> Update Care Giver </Button>
                            <Button color="primary" onClick={this.toggleInsertPatient}> Care Giver get Patient </Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <CareGiverTable tableData = {this.state.tableData}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                                            errorStatus={this.state.errorStatus}
                                                            error={this.state.error}
                                                        />   }
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Care Giver: </ModalHeader>
                    <ModalBody>
                        <CareGiverForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selectedDetele} toggle={this.toggleFormDelete}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormDelete}> Delete Care Giver: </ModalHeader>
                    <ModalBody>
                        <DeleteCareGiverForm reloadHandler={this.reloadDelete}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selectedUpdate} toggle={this.toggleFormUpdate}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormUpdate}> Update Care Giver: </ModalHeader>
                    <ModalBody>
                        <UpdateCareGiverForm reloadHandler={this.reloadUpdate}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selectedInsertPatient} toggle={this.toggleInsertPatient}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleInsertPatient}> Care Giver get Patient: </ModalHeader>
                    <ModalBody>
                        <PatientCareGiverForm reloadHandler={this.reloadInsertPatient}/>
                    </ModalBody>
                </Modal>

               </div>
        )

    }
}


export default CareGiverContainer;
