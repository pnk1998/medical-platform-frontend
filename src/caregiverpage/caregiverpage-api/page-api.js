import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
    caregiverShow:'/caregiver/findPacients/'
};

function getPatients(nume_caregiver, callback){
    let request = new Request(HOST.backend_api + endpoint.caregiverShow + nume_caregiver, {
       method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    getPatients
};