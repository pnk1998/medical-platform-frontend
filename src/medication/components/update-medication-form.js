import React from 'react';
import validate from "./validators/medication-validators"
import Button from "react-bootstrap/Button";
import * as API_USERS from "../api/medication-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';

class UpdateMedicationForm extends React.Component {

     constructor(props){
    super(props);
    this.toggleForm = this.toggleForm.bind(this);
    this.reloadHandler = this.props.reloadHandler;

       this.state = {

                   errorStatus: 0,
                   error: null,

                   formIsValid: false,

                   formControls: {
                       name_for_update: {
                           value: '',
                           placeholder: 'What is medication s name?...',
                           valid: false,
                           touched: false,
                           validationRules: {
                               minLength: 3,
                               isRequired: true
                           }
                       },
                       name: {
                           value: '',
                           placeholder: 'What is medication s name?...',
                           valid: false,
                           touched: false,
                           validationRules: {
                               minLength: 3,
                               isRequired: true
                           }
                       },
                       dosage: {
                           value: '',
                           placeholder: '1,2,3,4..10,...',
                           valid: false,
                           touched: false,
                       },
                       effects: {
                           value: '',
                           placeholder: 'Headache, stomachache',
                           valid: false,
                           touched: false,
                       },
                   }
               };

               this.handleChange = this.handleChange.bind(this);
               this.handleSubmit = this.handleSubmit.bind(this);
           }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    updateMedication(name_for_update,medication) {
        return API_USERS.putMedication(name_for_update, medication,(result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully updated medication with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {
            let medication = {
                name: this.state.formControls.name.value,
                dosage: this.state.formControls.dosage.value,
                effects: this.state.formControls.effects.value
            };
            let name_for_update = this.state.formControls.name_for_update.value;
            console.log(medication);
            this.updateMedication(name_for_update,medication);
        }

    render() {
            return (
                <div>
                    <FormGroup id='name_for_update'>
                        <Label for='name_for_updateField'> Name to update: </Label>
                        <Input name='name_for_update' id='name_for_updateField' placeholder={this.state.formControls.name_for_update.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.name_for_update.value}
                               touched={this.state.formControls.name_for_update.touched? 1 : 0}
                               valid={this.state.formControls.name_for_update.valid}
                               required
                        />
                        {this.state.formControls.name_for_update.touched && !this.state.formControls.name_for_update.valid &&
                        <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                    </FormGroup>

                    <FormGroup id='name'>
                        <Label for='nameField'> Name: </Label>
                        <Input name='name' id='nameField' placeholder={this.state.formControls.name.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.name.value}
                               touched={this.state.formControls.name.touched? 1 : 0}
                               valid={this.state.formControls.name.valid}
                               required
                        />
                        {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                        <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                    </FormGroup>

                    <FormGroup id='dosage'>
                        <Label for='dosageField'> Dosage: </Label>
                        <Input name='dosage' id='dosageField' placeholder={this.state.formControls.dosage.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.dosage.value}
                               touched={this.state.formControls.dosage.touched? 1 : 0}
                               valid={this.state.formControls.dosage.valid}
                               required
                        />
                    </FormGroup>

                    <FormGroup id='effects'>
                        <Label for='effectsField'> Effects: </Label>
                        <Input name='effects' id='effectsField' placeholder={this.state.formControls.effects.placeholder}
                               onChange={this.handleChange}
                               defaultValue={this.state.formControls.effects.value}
                               touched={this.state.formControls.effects.touched? 1 : 0}
                               valid={this.state.formControls.effects.valid}
                               required
                        />
                    </FormGroup>

                        <Row>
                            <Col sm={{size: '5', offset: 8}}>
                                <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.handleSubmit}>  Submit </Button>
                            </Col>
                        </Row>

                    {
                        this.state.errorStatus > 0 &&
                        <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                    }
                </div>
            ) ;
        }
}

export default UpdateMedicationForm;