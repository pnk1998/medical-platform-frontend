import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import DoctorForm from "./components/doctor-form";
import DeleteDoctorForm from "./components/delete-doctor-form";
import UpdateDoctorForm from "./components/update-doctor-form";

import * as API_USERS from "./api/doctor-api"
import DoctorTable from "./components/doctor-table";

class DoctorContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.toggleFormDelete = this.toggleFormDelete.bind(this);
        this.toggleFormUpdate = this.toggleFormUpdate.bind(this);

        this.reload = this.reload.bind(this);
        this.reloadDelete = this.reloadDelete.bind(this);
        this.reloadUpdate = this.reloadUpdate.bind(this);

        this.state = {
            selected: false,
            selectedDetele: false,
            selectedUpdate: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };

    }

    componentDidMount() {
        this.fetchDoctors();
    }

    fetchDoctors() {
        return API_USERS.getDoctors((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    toggleFormDelete(){
        this.setState({selectedDetele: !this.state.selectedDetele});
    }

    toggleFormUpdate(){
        this.setState({selectedUpdate: !this.state.selectedUpdate});
    }

    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchDoctors();
    }

    reloadDelete() {
        this.setState({
            isLoaded: false
        });
        this.toggleFormDelete();
        this.fetchDoctors();
    }

    reloadUpdate() {
            this.setState({
                isLoaded: false
            });
            this.toggleFormUpdate();
            this.fetchDoctors();
        }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Doctor Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm}>Add Doctor </Button>
                            <Button color="primary" onClick={this.toggleFormDelete}> Delete Doctor </Button>
                            <Button color="primary" onClick={this.toggleFormUpdate}> Update Doctor </Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <DoctorTable tableData = {this.state.tableData}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                                            errorStatus={this.state.errorStatus}
                                                            error={this.state.error}
                                                        />   }
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Doctor: </ModalHeader>
                    <ModalBody>
                        <DoctorForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selectedDetele} toggle={this.toggleFormDelete}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormDelete}> Delete Doctor: </ModalHeader>
                    <ModalBody>
                        <DeleteDoctorForm reloadHandler={this.reloadDelete}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selectedUpdate} toggle={this.toggleFormUpdate}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormUpdate}> Update Doctor: </ModalHeader>
                    <ModalBody>
                        <UpdateDoctorForm reloadHandler={this.reloadUpdate}/>
                    </ModalBody>
                </Modal>

               </div>

        )

    }
}


export default DoctorContainer;
