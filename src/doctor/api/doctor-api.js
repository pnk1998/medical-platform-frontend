import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
    doctor: '/doctor'
};

function getDoctors(callback) {
    let request = new Request(HOST.backend_api + endpoint.doctor, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getDoctorById(params, callback){
    let request = new Request(HOST.backend_api + endpoint.doctor + params.id, {
       method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postDoctor(user, callback){
    let request = new Request(HOST.backend_api + endpoint.doctor , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deleteDoctor(name,callback){
    let request = new Request(HOST.backend_api + endpoint.doctor+ '/' +name, {
       method: 'DELETE'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function putDoctor(name_for_update, user, callback){
    let request = new Request(HOST.backend_api + endpoint.doctor +'/' + name_for_update, {
       method: 'PUT',
       headers : {
                   'Accept': 'application/json',
                   'Content-Type': 'application/json',
               },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    getDoctors,
    getDoctorById,
    postDoctor,
    deleteDoctor,
    putDoctor
};